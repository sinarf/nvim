return {
  'stevearc/conform.nvim',
  config = function()
    local status_ok, conform = pcall(require, 'conform')
    if not status_ok then
      vim.notify 'Could not load module conform !'
      return
    end

    conform.setup {
      formatters_by_ft = {
        Jenkinsfile = {
          'npm-groovy-lint',
        },
        groovy = {
          'npm-groovy-lint',
        },
        lua = {
          'stylua',
        },
        python = {
          'ruff_format',
        },
        sh = {
          'shfmt',
        },
        markdown = {
          'prettier',
        },
      },
      formatters = {
        black = {
          prepend_args = {
            '--preview',
            '--unstable',
          },
        },
      },
    }

    local commander = require 'commander'
    local category = 'Format'
    local noremap = { noremap = true }
    commander.add {
      {
        desc = 'Format Document',
        cmd = function()
          conform.format {
            lsp_fallback = true,
            async = false,
            timeout_ms = 3000, -- ignored if async is true
          }
        end,
        keys = { { 'n', 'v' }, '<leader>lf', { noremap = true } },
        category = category,
      },
    }

    local format_on_save_group_name = 'format_on_save'
    -- Format on save
    local function enable_format_on_save()
      local group = vim.api.nvim_create_augroup(format_on_save_group_name, { clear = true })

      vim.api.nvim_create_autocmd('BufWritePre', {
        pattern = '*',
        callback = function(args)
          require('conform').format {
            bufnr = args.buf,
            lsp_fallback = true,
          }
        end,
        group = group,
      })
    end

    commander.add {
      {
        desc = 'Enable Format On Save',
        cmd = function()
          enable_format_on_save()
          vim.notify 'Format on Save ENABLED.'
        end,
        keys = { 'n', '<leader>la', noremap },
        category = category,
      },
      {
        desc = 'Disable Format On Save',
        cmd = function()
          vim.api.nvim_create_augroup(format_on_save_group_name, { clear = true })
          vim.notify 'Format on Save DISABLED.'
        end,
        keys = { 'n', '<leader>ld', noremap },
        category = category,
      },
    }

    vim.o.formatexpr = "v:lua.require'conform'.formatexpr()"
  end,
}
