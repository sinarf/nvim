return {
  'mfussenegger/nvim-lint',
  event = {
    'BufReadPre',
    'BufNewFile',
  },

  config = function()
    local status_ok, lint = pcall(require, 'lint')
    if not status_ok then
      vim.notify 'Could not load module lint!'
      return
    end

    lint.linters_by_ft = {
      groovy = {
        'npm-groovy-lint',
      },
      python = {
        'pylint',
      },
    }

    local lint_augroup = vim.api.nvim_create_augroup('lint', { clear = true })

    vim.api.nvim_create_autocmd({ 'BufEnter', 'BufWritePost', 'InsertLeave' }, {
      group = lint_augroup,
      callback = function()
        lint.try_lint()
      end,
    })
  end,
}
