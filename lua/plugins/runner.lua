return {
    "CRAG666/code_runner.nvim",
    opts = {
        filetype = {
            python = "python3 -u",
        },
    },
}
