return {
    "linux-cultist/venv-selector.nvim",
    dependencies = {
        "neovim/nvim-lspconfig",
        "mfussenegger/nvim-dap", "mfussenegger/nvim-dap-python", --optional
        {
            "nvim-telescope/telescope.nvim",
            branch = "0.1.x",
            dependencies = {
                "nvim-lua/plenary.nvim",
            }
        },
    },
    lazy = false,
    branch = "regexp", -- This is the regexp branch, use this for the new version
    config = function()
        require("venv-selector").setup()
        -- local group = vim.api.nvim_create_augroup("venv_autocmd", { clear = true })
        -- vim.api.nvim_create_autocmd('VimEnter', {
        --     desc = 'Auto select virtualenv Nvim open',
        --     group = group,
        --     pattern = '*',
        --     callback = function()
        --         local venv = vim.fn.findfile('pyproject.toml', vim.fn.getcwd() .. ';')
        --         if venv ~= '' then
        --             require('venv-selector').retrieve_from_cache()
        --         end
        --     end,
        --     once = true,
        -- })
    end,
}
