return {
  'danymat/neogen',
  config = function()
    local neogen = require 'neogen'
    neogen.setup()

    local commander = require 'commander'
    local noremap = { noremap = true }
    local category = 'neogen'

    commander.add {
      {
        desc = '',
        cmd = function()
          neogen.generate()
        end,
        keys = { 'n', '<leader>lc', noremap },
        category = category,
      },
    }
  end,
  version = '*',
}
