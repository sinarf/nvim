-- NOTE: avoid lazy loading as the autocmds may not be caught by nvim-rooter.lua.
return {
    'notjedi/nvim-rooter.lua',
    opts = {
        rooter_patterns = { '.git', '.hg', '.stfolder' },
        trigger_patterns = { '*' },
        manual = false,
        fallback_to_parent = false,
    },
}
