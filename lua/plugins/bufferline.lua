return {
  'akinsho/bufferline.nvim',
  version = '*',
  dependencies = 'kyazdani42/nvim-web-devicons',
  opts = {
    options = {
      buffer_close_icon = '󰅙',
      hover = {
        enabled = true,
        delay = 200,
        reveal = { 'close' },
      },
      custom_filter = function(buf_number)
        if vim.bo[buf_number].filetype ~= 'fugitive' then
          return true
        end
        if vim.bo[buf_number].filetype ~= 'gitcommit' then
          return true
        end
        if vim.bo[buf_number].filetype ~= 'alpha' then
          return true
        end
      end,
      diagnostics_indicator = function(count, level, _, _)
        local icon = level:match 'error' and ' ' or ' '
        return ' ' .. icon .. count
      end,
    },
  },
}
