return {
  'folke/trouble.nvim',
  dependencies = 'kyazdani42/nvim-web-devicons',
  opts = {
    modes = {
      diagnostics = {
        -- auto_open = true,
        auto_close = true,
      },
    },
  },
  keys = {
    { '<leader>td', '<cmd>Trouble diagnostics<cr>', desc = 'Trouble diagnostics' },
  },
}
