local on_attach = function(client, bufnr)
  -- NOTE: Remember that lua is a real programming language, and as such it is possible
  -- to define small helper and utility functions so you don't have to repeat yourself
  -- many times.
  --
  -- In this case, we createv a function that lets us more easily define mappings specific
  -- for LSP related items. It sets the mode, buffer and description for us each time.
  local nmap = function(keys, func, desc)
    if desc then
      desc = 'LSP: ' .. desc
    end

    vim.keymap.set('n', keys, func, { buffer = bufnr, desc = desc })
  end

  nmap('<leader>rn', vim.lsp.buf.rename, 'Rename')
  nmap('ga', vim.lsp.buf.code_action, 'Code Action')

  nmap('gd', vim.lsp.buf.definition, 'Goto Definition')
  nmap('gr', require('telescope.builtin').lsp_references, 'Goto Rreferences')
  nmap('gI', vim.lsp.buf.implementation, 'Goto Implementation')
  nmap('<leader>D', vim.lsp.buf.type_definition, 'Type Definition')
  nmap('<leader>ds', require('telescope.builtin').lsp_document_symbols, 'Document Symbols')
  nmap('<leader>ws', require('telescope.builtin').lsp_dynamic_workspace_symbols, 'Workspace Symbols')

  -- See `:help K` for why this keymap
  nmap('K', vim.lsp.buf.hover, 'Hover Documentation')
  nmap('<C-k>', vim.lsp.buf.signature_help, 'Signature Documentation')

  -- Lesser used LSP functionality
  nmap('gD', vim.lsp.buf.declaration, 'Goto Declaration')
  nmap('<leader>wa', vim.lsp.buf.add_workspace_folder, 'Workspace Add Folder')
  nmap('<leader>wr', vim.lsp.buf.remove_workspace_folder, 'Workspace Remove Folder')
  --     print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  -- nmap('<leader>wl', function()
  -- end, 'Workspace List Folders')

  -- Create a command `:Format` local to the LSP buffer
  vim.api.nvim_buf_create_user_command(bufnr, 'Format', function(_)
    vim.lsp.buf.format()
  end, { desc = 'Format current buffer with LSP' })
  if client.server_capabilities['documentSymbolProvider'] then
    require('nvim-navic').attach(client, bufnr)
  end
end

-- Enable the following language servers
--  Feel free to add/remove any LSPs that you want here. They will automatically be installed.
--
--  Add any additional override configuration in the following tables. They will be passed to
--  the `settings` field of the server config. You must look up that documentation yourself.
local servers = {
  bashls = {},
  html = {},
  jsonls = {},
  lua_ls = {
    Lua = {
      workspace = {
        checkThirdParty = false,
      },
      telemetry = {
        enable = false,
      },
      diagnostics = {
        -- Get the language server to recognize the `vim` global
        globals = { 'vim' },
      },
    },
  },
  lemminx = {},
  pylsp = {
    plugins = {
      yapf = { enabled = false },
      -- linter options
      pylint = { enabled = true },
      ruff = { enabled = false },
      pyflakes = { enabled = false },
      -- type checker
      pylsp_mypy = {
        enabled = true,
        report_progress = true,
        live_mode = false,
      },
      -- import sorting
      isort = { enabled = true },
      autopep8 = {
        enabled = true,
      },
      black = {
        enabled = true,
        preview = true,
      },
      jedi_completion = {
        fuzzy = true,
      },
      pycodestyle = {
        maxLineLength = 89,
      },
      flake8 = {
        enabled = true,
        extendIgnore = { 'E501' },
        maxLineLength = 89,
      },
      pyls_mypy = {
        enabled = true,
      },
      rope_autoimport = {
        enabled = true,
      },
    },
  },
  rust_analyzer = {},
  taplo = {}, -- toml files
  typos_lsp = {},
  vimls = {},
}

-- Setup neovim lua configuration
require('neodev').setup {
  library = {
    plugins = {
      'nvim-dap-ui',
      'neotest',
    },
    types = true,
  },
}

-- nvim-cmp supports additional completion capabilities, so broadcast that to servers
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

-- Setup mason so it can manage external tooling
require('mason').setup()
require('mason-update-all').setup()
require('mason-tool-installer').setup {
  ensure_installed = {
    'black',
    'debugpy',
    'gitleaks',
    'gitlint',
    'isort',
    'npm-groovy-lint',
    'prettier',
    'pydocstyle',
    'pylint',
    'ruff',
    'shellcheck',
    'stylua',
  },

  auto_update = true,
  run_on_start = true,
  start_delay = 3000,
}
-- Ensure the servers above are installed
local mason_lspconfig = require 'mason-lspconfig'

mason_lspconfig.setup {
  ensure_installed = vim.tbl_keys(servers),
  automatic_installation = true,
}

mason_lspconfig.setup_handlers {
  function(server_name)
    require('lspconfig')[server_name].setup {
      capabilities = capabilities,
      on_attach = on_attach,
      settings = servers[server_name],
    }
  end,
}

-- Turn on lsp status information
require('fidget').setup()

local signs = {
  { name = 'DiagnosticSignError', text = '' },
  { name = 'DiagnosticSignWarn', text = '' },
  { name = 'DiagnosticSignHint', text = '' },
  { name = 'DiagnosticSignInfo', text = '' },
  { name = 'DapBreakpoint', text = '' },
  { name = 'DapBreakpointCondition', text = '' },
  { name = 'DapStopped', text = '' },
}

for _, sign in ipairs(signs) do
  vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.text, numhl = '' })
end
require('lspconfig').sourcekit.setup {}
require('lspconfig').lemminx.setup {
  filetypes = { 'xml', 'xsd', 'xsl', 'xslt', 'svg', 'ant' },
}
