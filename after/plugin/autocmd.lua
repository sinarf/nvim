local lsp_group = vim.api.nvim_create_augroup('lsp', { clear = true })
vim.api.nvim_create_autocmd('User', {
  group = lsp_group,
  pattern = 'MasonUpdateAllComplete',
  callback = function()
    vim.notify 'mason-update-all has finished'
  end,
})

local global_group = vim.api.nvim_create_augroup('global_autocmd', { clear = true })
-- Always checks if a file has been changed when entering a buffer.
vim.api.nvim_create_autocmd('BufEnter', {
  group = global_group,
  pattern = '*',
  command = 'checktime',
})
vim.api.nvim_create_autocmd('TextYankPost', {
  group = global_group,
  callback = function()
    vim.highlight.on_yank {
      higroup = 'IncSearch',
      timeout = 200,
    }
  end,
})
vim.api.nvim_create_autocmd('BufEnter', {
  group = global_group,
  pattern = {
    '/*_ci/*.groovy',
    'Jenkinsfile*',
  },
  command = 'set ft=Jenkinsfile',
})
vim.api.nvim_create_autocmd('BufEnter', {
  group = global_group,
  pattern = {
    '*.lst',
  },
  command = 'set ft=txt',
})
