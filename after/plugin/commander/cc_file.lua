local command_center = require 'commander'
local noremap = { noremap = true }
local category = 'file'

command_center.add {
  {
    desc = 'Make executable',
    cmd = '<cmd>!chmod +x %<CR>',
    category = category,
  },
  {
    desc = 'Undotree toggle',
    cmd = '<cmd>UndotreeToggle<CR>',
    keys = { 'n', '<leader>ut', noremap },
    category = category,
  },
  {
    desc = 'Format file with idea',
    cmd = '<cmd>split |resize 5| terminal  format_idea.sh %<cr>',
    category = category,
  },
  {
    desc = 'Open parent directory',
    cmd = '<CMD>Oil<CR>',
    keys = { 'n', '-', noremap },
    category = category,
  },
}
