local command_center = require 'commander'
local category = 'python'
local noremap = { noremap = true }

command_center.add {
  {
    desc = 'Sync pipenv virtual env',
    cmd = '<cmd>!pipenv-start.sh<cr>',
    keys = { 'n', '<leader>pvs', noremap },
    category = category,
  },
  {
    desc = 'Install pipenv dev virtual env',
    cmd = '<cmd>split | resize 10 | terminal pipenv --rm || true && pipenv install --dev<cr>',
    keys = { 'n', '<leader>pvs', noremap },
    category = category,
  },
  {
    desc = 'Select virtual env',
    cmd = '<cmd>VenvSelect<cr>',
    keys = { 'n', '<leader>pvs', noremap },
    category = category,
  },
  {
    desc = 'Deactivate virtual env',
    cmd = function()
      require('venv-selector').deactivate()
    end,
    keys = { 'n', '<leader>pvd', noremap },
    category = category,
  },
  {
    desc = 'List of pylint messages',
    cmd = '<cmd>e $TEMP/pylint-messages.txt | .! pylint --list-msgs<CR>',
    category = category,
  },
  {
    desc = 'Organize Import in current file',
    cmd = '<cmd>split | resize 10 | terminal cmd>!isort %<CR>',
    keys = { 'n', '<leader>poi', noremap },
    category = category,
  },
  {
    desc = 'Organize Import in current project',
    cmd = '<cmd>split | resize 10 | terminal isort .<CR>',
    category = category,
  },
  {
    desc = 'Format with black current file',
    cmd = '<cmd>split | resize 10 | terminal black --preview %<CR>',
    category = category,
  },
  {
    desc = 'Format with black current project',
    cmd = '<cmd>split | resize 10 | terminal black --preview .<CR>',
    category = category,
  },
}
