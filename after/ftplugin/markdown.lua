local opts = { noremap = true, silent = true }

vim.keymap.set('n', '<localleader>d', ':MDTaskToggle<CR>', { desc = 'Toggle Todo', unpack(opts) })
vim.keymap.set('n', '<localleader>p', ':MarkdownPreview<CR>', { desc = 'Markdown Preview', unpack(opts) })

